interface TitleProps{
    title:string,
    subtitle:string,
    sizeTextTitle?:number,
}

const Title=({title,subtitle,sizeTextTitle}:TitleProps)=>{
    return(
        <div className="title">
            <h4 style={{textTransform:"uppercase"}}>{subtitle}</h4>
            <h3 style={{fontSize:sizeTextTitle?`${sizeTextTitle}px`:"30px"}}>{title}</h3>
        </div>
    )
}

export default Title