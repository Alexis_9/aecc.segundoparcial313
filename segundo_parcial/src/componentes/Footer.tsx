interface itemFooterProps {
    title: string;
    items: string[];
}
const ItemFooter = (props:itemFooterProps) => {
    return (
        <div>
            <h6>{props.title}</h6>
            <ul>
                {props.items.map((item:string,index:number)=>{
                    return <li key={index}>
                        <a href="#">{item}</a>
                    </li>
                })}
            </ul>
        </div>
    )
}