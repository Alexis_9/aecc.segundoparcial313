import Button from './Button'
import Logo from './Logo'
import Link from 'next/link'

const styleNav={
    // backgroundColor:"blue"
}
export function Header(){
    return (
        <div className="container nav-header" style={styleNav}>
            <Logo/>
            <ul>
                <li>
                    <Link href="/" className='active'>Home</Link>
                </li>
                <li>
                    <Link href="/">abaout</Link>
                </li>
                <li>
                    <Link href="/">prueba1</Link>
                </li>
                <li>
                    <Link href="/">prieba2</Link>
                </li>
                <li>
                    <Link href="/">prueba 3</Link>
                </li>
            </ul>
            <div>
                <Button title="Log In" transparent={true}/>
                <Button title="Sign Up" primary/>
            </div>
        </div>
    )
}