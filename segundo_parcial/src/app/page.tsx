import Image from "next/image";
import Button from "@/componentes/Button";
import Logo from "@/componentes/Logo";
import Title from "@/componentes/Title";
import Footer from "@/componentes/Footer";

export default function Home() {
  return (
    <main>
      <div className="encabezado">
        <div className="arriba">
           <Image src="/imagenes/Vectorfig_1.png" alt="" width={100} height={100}/>
           <h1>Ik developers</h1>
        </div>
      <div className="link">
        <ul >
          <li >About us</li>
          <li>Services</li>
          <li>Case Studies</li>
          <li>Blog</li>
          <li>How it Works</li>
          <li >Hire </li>
          
        </ul>
      </div>
      <div className="boton">
        <Button title="Contact us"></Button>
        </div>
      </div>
      <div className="imgp">
        <div className="parrafo">
        <h1>Great <span className="multico">Product</span>  is  <br></br> <span className="negrita">built by great </span> <span className="multico">teams</span></h1>
        <p>We help build and manage a team of world-class developers <br></br> to bring your vision to life </p>  
        <Button title="Let’s get started!" color="azul"> </Button>
        </div>
        <div>
        < Image src="/imagenes/trap.png" alt="" width={100} height={100} />
        </div>
      </div>
      <div className="barra_intermedia">
        <div className="parrafo_barra">
        <h1>Helping a local <br /> <span className="multico_2">business reinvent itself</span> </h1>
        <p>We reached here with our hard work and dedication </p>  
        </div>
      <div className="teximg">
      <div className="pruebba"> <Image src="/imagenes/soccial.png" alt="" width={100} height={100} /> 
      <Image src="/imagenes/manos.png" alt="" width={100} height={100}  />
      </div>  <h3>222.222</h3><p>Membres</p>
      <div className="pru"> <Image src="/imagenes/clik.png" alt=""  width={100} height={100} /> 
       </div>
      </div>
      </div>
      <div className="parr">
        <Image  src="/imagenes/Deco-lineline.png" alt="" width={100} height={100} />
        <h1>Meet the People  <br /> <span className="negrita">We are Working With</span> </h1>
      </div>
      <div className="carucel">
      <Image src="/imagenes/logo9.png" alt=""  width={100} height={100} />
      <Image  src="/imagenes/logo8.png" alt=""  width={100} height={100} />
      <Image src="/imagenes/logo3.png" alt=""   width={100} height={100}/>
      <Image  src="/imagenes/Logo-6.png" alt=""   width={100} height={100}/>
      <Image  src="/imagenes/Logo-7.png" alt=""   width={100} height={100}/>
      <Image  src="/imagenes/hd.png" alt=""   width={100} height={100}/>
      </div>
      <div className="tele1">
        <div className="telef">
        <Image  src="/imagenes/cas1.png" alt=""  width={100} height={100} />
        </div>
        <div className="prr">
          <h6>Website Design for SCFC Canada</h6> <p>Born out of a vision, a single-minded objective that puts service before <br />
          anything else, Swift Clearance and Forwarding Corp. surging forth to deliver <br />
          the best services in the shipping and logistics scenario. Its meteoric rise stems <br />
          out of a solid foundation. The management boasts of over 20 years of rich and <br />
         varied experience in the shipping and freight forwarding industry.</p>
        </div>
      </div>
      <div className="tele1">
        <div className="telef">
        <Image src="/imagenes/cas2.png" alt=""  width={100} height={100} />
        </div>
        <div className="prr">
          <h6>Website Design for SCFC Canada</h6> <p>Born out of a vision, a single-minded objective that puts service before <br />
          anything else, Swift Clearance and Forwarding Corp. surging forth to deliver <br />
          the best services in the shipping and logistics scenario. Its meteoric rise stems <br />
          out of a solid foundation. The management boasts of over 20 years of rich and <br />
         varied experience in the shipping and freight forwarding industry.</p>
        </div>
      </div>
      <div className="tele1">
        <div className="telef">
        <Image src="/imagenes/cas3.png" alt="" width={100} height={100}  />
        </div>
        <div className="prr">
          <h6>Website Design for SCFC Canada</h6> <p>Born out of a vision, a single-minded objective that puts service before <br />
          anything else, Swift Clearance and Forwarding Corp. surging forth to deliver <br />
          the best services in the shipping and logistics scenario. Its meteoric rise stems <br />
          out of a solid foundation. The management boasts of over 20 years of rich and <br />
         varied experience in the shipping and freight forwarding industry.</p>
        </div>
      </div>
      <div className="parroo">
         src="/imagenes/Deco-lineline.png" alt="" />
        <h1>Way of building <br /> <span className="negrita">We are Working With</span> </h1>
      </div>
      <div className="final">
        <div className="fin">
          <h5>UX Driven Engineering</h5><p>Unlike other companies, we are a UX first development company. <br />
          Projects are driven by designers and <br />
          they make sure design and experiences translate to code.</p>
        </div>
        <div className="fin">
          <h5>Developing Shared Understanding</h5> <p>Unlike other companies, we are a UX first development company. <br />
          Projects are driven by designers and they make sure design and <br />
          experiences translate to code.</p>
        </div>
      </div>
      <div className="final">
        <div className="fin">
          <h5>Proven Experience and Expertise</h5><p>Unlike other companies, we are a UX first development company. <br />
          Projects are driven by designers and <br />
          they make sure design and experiences translate to code.</p>
        </div>
        <div className="fin">
          <h5>Security & Intellectual Property (IP)</h5> <p>Unlike other companies, we are a UX first development company. <br />
          Projects are driven by designers and they make sure design and <br />
          experiences translate to code.</p>
        </div>
        </div>
        <div className="final">
        <div className="fin">
          <h5>Code Reviews</h5><p>Unlike other companies, we are a UX first development company. <br />
          Projects are driven by designers and <br />
          they make sure design and experiences translate to code.</p>
        </div>
        <div className="fin">
          <h5>Quality Assurance & Testing</h5> <p>Unlike other companies, we are a UX first development company. <br />
          Projects are driven by designers and they make sure design and <br />
          experiences translate to code.</p>
        </div>
      </div>
    </main>
  );
}
